/**
 *
 * 		present.c
 *
 * 		Created by Eduardo Novella and Thom Wiggers on 4/17/14.
 *
 * 		Based on C code by Dirk Klose
 *
 *
 *      Key length   = 80 bits
 *      Block length = 64 bits
 *      Rounds       = 31
 *      Sbox         = 4 bits (1 nibble)  F_{2^4}
 *
 */

// TODO  Find out where we are extracting random data from (atmega163 )

//#include <avr/io.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

// Use this debug mode to take a look at traces
#define DEBUG 1

// Number of rounds for PRESENT 80-bit key
#define ROUNDS 32

// Substitution Boxes
//                              0   1   2   3   4   5   6   7   8   9   a   b   c   d   e   f
const uint8_t Sbox[16]     = {0xc,0x5,0x6,0xb,0x9,0x0,0xa,0xd,0x3,0xe,0xf,0x8,0x4,0x7,0x1,0x2};
const uint8_t Sbox_inv[16] = {0x5,0xe,0xf,0x8,0xc,0x1,0x2,0xd,0xb,0x4,0x6,0x3,0x0,0x7,0x9,0xa};

// Permutations are calculated on-the-fly

// WATCH OUT==>Little-endian!
// Key length   = 10 bytes = 80 bits
//uint8_t key[]   = {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};

// KEY = CAFEBABEDEADCODEFABA
uint8_t key[]   =   {0xBA,0xFA,0xDE,0xC0,0xAD,0xDE,0xBE,0xBA,0xFE,0xCA};

// Plaintext blocks 8 bytes = 64 bits
// STATE = 11000000000000FF
uint8_t state[] = {0xFF,0x00,0x00,0x00,0x00,0x00,0x00,0x11};

// Cyphertext
uint8_t encrypted[8];

// Matrix for all roundkeys. Rounkeys are 8 bytes [9:2], so 2 bytes are discarded
uint8_t roundkeys[ROUNDS][10];


//------------------------------------------
// Printout functions ( Debug mode only)
//------------------------------------------
void showRoundKey(uint8_t round){
	uint8_t i;
	printf("Roundkey: %02d  ",round+1);
	for (i=9;i>=2;i--)
		printf("%02x",roundkeys[round][i]);
	printf("\n");
}

void showState(){
	printf("State: ");
    	uint8_t i;
	for (i=8;i>0;i--)
		printf("%02x",state[i-1]);
	printf("\n");
}

void showEncrypted(uint8_t round){
	printf("State: ");
   	uint8_t i;
	for (i=8;i>0;i--)
		printf("%02x",encrypted[i-1]);
	printf("\n");
}

//-----------------------------------------------------
//  Just a copy
//-----------------------------------------------------
void copyStateToEncrypted(){
    uint8_t i;
    for (i=0;i<8;i++)
        encrypted[i] = state[i];
}

//-----------------------------------------------------
// Key Scheduling
//-----------------------------------------------------
void generateRoundKeys(uint8_t key[]){
    uint8_t tmp1,tmp2;
	uint8_t round,i;
    uint8_t init_key[] = {0xBA,0xFA,0xDE,0xC0,0xAD,0xDE,0xBE,0xBA,0xFE,0xCA};

    for (i = 0; i < 10; i++) {
      key[i] = init_key[i];
    }

	//----------------
	// Initial key
	//----------------
	for (i=0;i<10;i++)
		roundkeys[0][i]=key[i];

	#ifdef DEBUG
		printf("[+] Generating round keys:\n");
		showRoundKey(0);
	#endif

	for ( round = 1 ; round < ROUNDS; round++){
		//--------------------------------------------------------------------
		// key = k79k78.....k0 . The 64-bit round key Ki = k63 k62 . . . k0
		// consists of the 64 leftmost bits of the current contents of key
		//--------------------------------------------------------------------
		tmp1  = key[0];
		tmp2  = key[1];
		for (i=0; i<8;i++)
			key[i] = key[i+2];

		key[8] = tmp1;
		key[9] = tmp2;

		//---------------------------------------------------------------------
		// Shift (the key register is rotated by 61 bit positions to the left)
		//---------------------------------------------------------------------
		tmp1 = key[0] & 7;
		for (i=0;i<9;i++)
			key[i] = key[i] >> 3 | key[i+1] << 5;

		key[9] = key[9] >> 3 | tmp1 << 5;

		//---------------------------------------------------------------------------------
		// S-Box application (the left-most four bits are passed through the present S-box)
		//---------------------------------------------------------------------------------
		key[9] = Sbox[key[9]>>4]<<4 | (key[9] & 0xF);

		//-----------------------------
		//round counter addition
		//-----------------------------
		if((round) % 2 == 1)
			key[1] ^= 128;
		key[2] = ((((round)>>1) ^ (key[2] & 15)) | (key[2] & 240));

		//-------------------------------
		//  Storage into a matrix
		//-------------------------------
		for (i=0;i<10;i++)
			roundkeys[round][i]=key[i];


		#ifdef DEBUG
			showRoundKey(round);
		#endif
	}

	#ifdef DEBUG
		printf("\n");
	#endif
}

//-----------------------------------------------------
// Main functions in PRESENT
// addRoundKey && Sboxes && pLayer
//-----------------------------------------------------

void addRoundKey(uint8_t round, uint8_t enc){
	// consists of the 64 leftmost bits of the current contents of register Key
	//----------------
	// Shift >>16
	//----------------
    uint8_t k;
	if (enc){
		for (k=0; k < 8; k++)
			state[k] = state[k] ^ roundkeys[round][k+2];
	}else{
		for (k=0; k < 8; k++)
			encrypted[k] = encrypted[k] ^ roundkeys[round][k+2];
	}


	#ifdef DEBUG
		showRoundKey(round);
		printf("After addRoundKey:");
		if (enc)
			showState();
		else
			showEncrypted(round);
	#endif
}


// TODO test
void random_sort(uint8_t list[], uint8_t random[]) {
	// random is an array with random values of the same
	// length as list[].
	uint8_t j, temp;
	for (uint8_t i = 1; i < sizeof(list); i++) {
		j = i;
		while (j > 0 && random[j-1] > random[j]) {
			// swap
			temp = list[j];
			list[j] = list[j-1];
			list[j-1] = temp;
			temp = random[j];
			random[j] = random[j-1];
			random[j-1] = temp;

			j--;
		}
	}
}

void sBoxLayer(uint8_t round, uint8_t enc){
	uint8_t i;
	uint8_t j[] = {8, 7, 6, 5, 4, 3, 2, 1, 0},
			r[] = {rand()%256, rand()%256, rand()%256, rand()%256,
					   rand()%256, rand()%256, rand()%256, rand()%256};
	random_sort(j, r);

	if (enc){
		for ( i=0; i <= 8; i++) {
			state[j[i]] = Sbox[state[j[i]]>>4]<<4 | Sbox[state[j[i]] & 0xF];
		}
	}else{
		for ( i=0 ; i<=8 ;i++)
			encrypted[j[i]] = Sbox_inv[encrypted[j[i]]>>4]<<4 | Sbox_inv[encrypted[j[i]] & 0xF];
	}

	#ifdef DEBUG
		printf("After SboxLayer  :");
		if (enc)
			showState();
		else
			showEncrypted(round);
	#endif
}

void pLayer(uint8_t round, uint8_t enc){
	uint8_t i           = 0;
	uint8_t position    = 0;
	uint8_t element_src = 0;
	uint8_t bit_src     = 0;
	uint8_t element_dst = 0;
	uint8_t bit_dst     = 0;
	uint8_t tmp_pLayer[8];

	for(i=0;i<8;i++)
		tmp_pLayer[i] = 0;

	for(i=0;i<64;i++) {
		//---------------------------------
		// Create permutation lookup table
		//---------------------------------
		if (enc)
			position = (16*i) % 63;
		else
			position = (4*i) % 63;
		if(i == 63)		//exception for bit 63
			position = 63;
		//---------------------------------
		// Permute bit to bit
		//---------------------------------
		element_src = i / 8;
		bit_src     = i % 8;
		element_dst = position / 8;
		bit_dst     = position % 8;
		if (enc)
			tmp_pLayer[element_dst] |= ((state[element_src]>>bit_src) & 0x1) << bit_dst;
		else
			tmp_pLayer[element_dst] |= ((encrypted[element_src]>>bit_src) & 0x1) << bit_dst;
	}

	//------------------
	// Update register
	//------------------
	for(i=0;i<=7;i++){
		if (enc)
			state[i]     = tmp_pLayer[i];
		else
			encrypted[i] = tmp_pLayer[i];
	}

	#ifdef DEBUG
		printf("After pLayer     :");
		if (enc)
			showState();
		else
			showEncrypted(round);
	#endif
}


//-----------------------------------------------------
// Encrypt && decrypt
//-----------------------------------------------------
void encrypt(){
	#ifdef DEBUG
		printf("\n\n[+] Encrypting:\n");
	#endif

	uint8_t round;
	for ( round=0 ; round < ROUNDS-1 ; round++){
		addRoundKey(round,1);
		sBoxLayer(round,1);
		pLayer(round,1);
	}
	addRoundKey(ROUNDS-1,1);

}

void decrypt(){
	#ifdef DEBUG
		printf("\n\n[+] Decrypting:\n");
	#endif

	uint8_t round;
	for ( round=ROUNDS-1 ; round >0  ; round--){
		addRoundKey(round,0);
		pLayer(round,0);
		sBoxLayer(round,0);
	}
	addRoundKey(0,0);
}

// TODO Test
void encrypt_present(char buffer[], char out[]) {
	for (uint8_t i = 0; i < 8; i++) {
		state[7-i] = buffer[i];
	}
	generateRoundKeys(key);
	encrypt();

	for (uint8_t i = 0; i < 8; i++) {
		out[i] = state[7-i];
	}
}

/*
// Seed the random number generator
void seed_PRNG(void) {
	// copied from
	// http://bytecruft.blogspot.nl/2013/03/seeding-standard-c-random-number-generator.html

	unsigned char oldADMUX = ADMUX;
	ADMUX  |=  1 << MUX0; //choose ADC1 on PB2
	ADCSR |=  (1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0); //set prescaler to max value, 128

	ADCSR |= (1 << ADEN); //enable the ADC
	ADCSR |= (1 << ADSC);//start conversion

	while (ADCSR & (1 << ADSC)); //wait until the hardware clears the flag. Note semicolon!

	unsigned char byte1 = ADCL;

	ADCSR |= (1 << ADSC);//start conversion

	while (ADCSR & (1 << ADSC)); //wait again note semicolon!

	unsigned char byte2 = ADCL;

	unsigned int seed = byte1 << 8 | byte2;

	ADMUX = oldADMUX;

	srand(seed);
}*/


//-----------------------------------------------------
// Main
//-----------------------------------------------------
int main(int argc, char* argv[]){

	//seed_PRNG();


    //char foo[] = {0x2e, 0x84, 0x0a, 0xd1, 0xf4, 0x79, 0x5c, 0x95};
    //char foo[] = {0x95, 0x5c, 0x79, 0xf4, 0xd1, 0x0a, 0x84, 0x2e};
    char foo[] = {0x2d, 0xdb, 0xea, 0x24, 0x4d, 0x74, 0x8c, 0x38};
    char bar[8];
    encrypt_present(foo, bar);
  //  copyStateToEncrypted();
  //  decrypt();
//    showEncrypted(NULL);
}
