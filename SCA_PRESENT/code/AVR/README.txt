Project--> Configuration Options --> Custom  ---------> Point out where are arv-gcc & make binaries --> c:/WINAVR2010...../bin and c:/WINAVR2010...../utils/bin


Build started 8.6.2014 at 12:23:31
avr-gcc  -mmcu=atmega163 -Wall -gdwarf-2 -Os -std=gnu99 -funsigned-char -funsigned-bitfields -fpack-struct -fshort-enums -MD -MP -MT AVR_present.o -MF dep/AVR_present.o.d  -c  ../AVR_present.c
avr-gcc -mmcu=atmega163 -Wl,-Map=AVR_present.map AVR_present.o     -o AVR_present.elf
avr-objcopy -O ihex -R .eeprom -R .fuse -R .lock -R .signature  AVR_present.elf AVR_present.hex
avr-objcopy -j .eeprom --set-section-flags=.eeprom="alloc,load" --change-section-lma .eeprom=0 --no-change-warnings -O ihex AVR_present.elf AVR_present.eep || exit 0
avr-objdump -h -S AVR_present.elf > AVR_present.lss
Build succeeded with 0 Warnings...
