
#include <inttypes.h>
#include <stdio.h>
#include "present.h"
#include <time.h>
#include <stdlib.h>

int main(void) {
    int i;
    const uint8_t input[8]  = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
    const uint8_t key[10]  = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};
    uint8_t output[8] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

    printf("hi\n");
    seed_PRNG();
    encrypt_present(input, key, output, SHUFFLE_SBOX);
    encrypt_present(input, key, output, SHUFFLE_SBOX | SHUFFLE_PERMUTATION);
    printf("OUTPUT:");
    for (i = 0; i < 8; i++) {
        printf(" %.2X", output[i]);
    }
    printf("\n");
    printf("EXPECT: E7 2C 46 C0 F5 94 50 49\n");
    return 0;
}
