\documentclass{article}

\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsfonts}
\usepackage{hyperref}
\usepackage{microtype}
\usepackage{xspace}

\newcommand{\quark}{\textsc{Quark}\xspace}
\newcommand{\uquark}{\textsc{uQuark}\xspace}
\renewcommand{\and}{\textsc{and}\xspace}
\newcommand{\xor}{\textsc{xor}\xspace}

\title{Speed: uQuark implementation in AVR assembly \\ 
  \large Cryptographic Engineering}

\author{Eduardo Novella Lorente \& Thom Wiggers}

\begin{document}
\maketitle

\section{Introduction}

We implemented the \uquark lightweight hash algorithm defined by Aumasson et
al\cite{quark} on an \emph{ATTiny45} AVR. We focussed on maximizing the speed
of the implementation, that is, using as few cycles as possible to compute
the hash. \uquark is the smallest version of \quark, a sponge-style
hashing algorithm. \uquark produces a hash sized 136 bits (17 bytes).

Nevertheless, the authors of the \quark paper wrote a very optimized and
scalable version of \uquark in their C-implementation and our understanding of
C code was limited, we first coded a C version of the \uquark cipher.
Afterwards, this was ported to assembly. We will discuss the assembly
implementation below.

\section{Features}

As the table shows below, there are 3 flavours of this cryptographic algorithm.
We decided to implement the lightest version. Those units are in bits:\\

\begin{center}

\begin{tabular}{| c | c | c | c | c|c|}
\hline
Instance & {\bf Rate} & {\bf Capacity} & {\bf Width} &{\bf Rounds}&{\bf Digest}\\
	\hline
        & (r)& (c)  & (b)  & (4b) & (n)\\
u-Quark & 8  & 128  & 136  & 544  & 136\\
d-Quark & 16 & 160  & 176  & 704  & 176\\
s-Quark & 32 & 224  & 256  & 1024 & 256\\
	\hline
\end{tabular}

\end{center}

\section{Implementation}

An important feature of the \quark algorithm, which being a sponge hash, has a specific construction with various
phases. We will describe our implementation of each of these phases one at
a time later in this report.

The central element of the \quark algorithm is the state. This global state is
first initialized from an IV, and then various operations are performed on
(parts of the) state. The state is kept in two NFSRs, non-linear feedback shift
registers. These registers, $X$ and $Y$, are both 68 bits long in \uquark.



\subsection{Initialization}
For the initialization we need 2 mandatory requirements:
\begin{itemize}
	\item An Initial Vector (IV) with 17 bytes long, which it will be the initial state to begin. 
	\item A plaintext or message, which will be padded following some steps described below.
\end{itemize}  

Point out what we used default data both with message as the IV. We decided to
load the IV directly to registers. Even though the message was only a byte
(0x80), we decided to load the message into SRAM because we have a limited
amount of limited registers, and also because that would be a more realistic
source. For these steps, we used 2 macros.

\subsubsection{Padding}

In this phase we have used an empty message as input to \uquark. Principally
because we obtained the cipher test vector in the official paper. A message is
padded by appending ``$1$'' followed by the minimal amount of ``$0$'' bits to
reach a length multiple of its rate (8 bits). A padded empty message looks like
$10000000$ ($0x80$).

\subsection{Permutation}

Before get started talking about absorbing or squeezing is better to see how
the permutation works. The permute-function is built around three feedback
shift registers (FSR) (Figure~\ref{fig:permutation}). These registers are each
clocked in a different way. Registers $X$ and $Y$ both contain half of the
state. For the permute phase, we put these two registers both in the chip's
registers. We stow $X$ in $X_0\cdots X_8$ and $Y$ in $Y_0\cdots Y_8$. Because
$\frac{68}{8} = 8.5$, we use nine registers per NFSR. We align them such that
the first four bits in $X$ and $Y$ are in positions $3, 2, 1, 0$ in $X0$ and
$Y0$, respectively. The third linear shift register is $L$. $L$ is
10 bits long and initialized with \texttt{0x3FF} (10 1's) every time we start
the permuting. $L$ is rotated each round with as new bit the result of $L[0]
\oplus L[3]$.

\begin{figure}[h!tb]
 \begin{center}
  \includegraphics[scale=0.35]{img/NFSR.png}
 \end{center}
 \caption{Diagram of the permutation of Quark.\cite{quark}}
\label{fig:permutation}
\end{figure}

$X$ is first shifted left. The new bit that should be inserted at the end of
$X$ is the result of $f(X) \oplus Y[0] \oplus H(X, Y)$. Because the four most
significant bits of $X0$ weren't used initially, we can safely rotate everything
in $X$ one to the left ahead of having the result of this exclusive or: we are
still able to access every bit in $X$, they just end up in one bit to the left.

We first calculate the result of $f$. For details of $f$, please see
\quark\cite{quark}. We do this bitwise, loading a single bit from $X$ and then
storing that bit in the \textsc{LSB} of a temporary register $T$. We then do
$X8 \oplus T$. Because only the \textsc{LSB} of $T$ is set, this can only
toggle the lowest bit of $X8$, which is where we are storing our result.

There are several large \textsc{and} operations in function $f$. These
operations largely access the same bits of $X$ as the individual \textsc{xor}s.
Because we want to avoid loading things twice, we store reused bits into
a temporary storage $S$ register on specified indices. For the \textsc{and}
operations, we then load $T$ with $1$'s on the indices of these bits and then
calculate $T \& S$. We then check if $T$ changed from its initial
value\footnote{we do this in a time-invariant way instead of using
  \texttt{CPI}}: if it did, then some bits in $S$ weren't set and we know that
the result of the operation is $0$. Because $S$ was slightly too small to
hold all bits used in \textsc{and}s, we sometimes then still need to
calculate the \textsc{and} of them with the result of the previous operation.
These cases only need one additional bit, and thus only one additional
operation.

After calculating $f$, we \textsc{xor} the result with $Y[0]$. We've then
done two-thirds of our manipulation of $X$.

$Y$ is shifted left and then $g$ is calculated similarly to $f$. It is a little
more complicated because the \textsc{and}s involve a few more bits, but otherwise
it's exactly the same.

$h$ is then calculated. This function also uses the same approach as before.

These operations are then repeated 544 times.

\subsection{Absorbing}

In this phase, 2 steps are clearly defined : a \xor and permutation. An initial
\xor operation by using: message (1 byte) $\oplus$ last byte of state (r in the
picture~\ref{fig:quark}. We appreciate that $IV/state = capacity(c) + rate(r)
=16+1 bytes = 136 bits$). After that \xor operation a permutation with 544
rounds is accomplished. Different versions of \quark differ principally in how
many rounds and \xor operations must be done.

\begin{figure}[h!tb]
 \begin{center}
  \includegraphics[scale=0.35]{img/quark.png}
 \end{center}
 \caption{The sponge construction as used by \quark, for the example of a 4-block (padded) message.\cite{quark}}
\label{fig:quark}
\end{figure}

\subsection{Squeezing}
	
This is the step where a hashed \uquark digest is obtained like output value
from the algorithm.  In this phase, data is ``squeezed'' by using a permutation
during 17 times to extract every time a byte to construct the final hash value.
This function is storing after every permutation one byte in SRAM from the
offset 0x0131 to 0x0141. Finally we are able to see the expected value in SRAM
after a execution of the algorithm. For more details, we have attached several
log files to match with or just take a look at annexes~\cite{quark}. \\

An evidence which claims that our algorithm is working properly can be found at
the following address:

{\small{
\begin{center}
\begin{tabular}{| c | c | c |}
\hline
Padded message &  \uquark hash & SRAM offset\\
	\hline
 0x80 &{\bf126b75bcab23144750d08ba313bbd800a4}& 0x0131...0x0141\\ 
	\hline
\end{tabular}
\end{center}
}


\section{Further possible improvements}

The main part of the algorithm is the permutation phase: most cycles are spent
here. \quark is constructed such that the lower 8 bits of $X$ and $Y$ are never
accessed during a single permute. This means that it is possible to grab whole
bytes, containing for instance $X[0]_t,X[0]_{t+1},\cdots,X[0]_{t+7}$ at a time.
This is possible by shifting in smart increments, extracting bytes when they
align right. This does require some more temporary registers, but would also
achieve 8-times parallelism. However, we sadly did not have time to implement this.

From the beginning we get started allowing different lengths of messages, and
we also designed the padding function. But it was a bit buggy and it was not
the real idea of this assignment. Hence, finally we decided to use directly the
padded message following the example of the paper. It would be interesting to
add this feature if this code is reused or must be improved.

\section{Conclusion}

Implementing \quark on a limited architecture was an interesting experience. We
realized that the AVR architecture is quite generous with register and were
able to exploit that fact in our implementation. It still was quite a lot of
work though: especially debugging assembly code which is juggling bits proved
to be quite a challenge. Tracking a small bug in the $f$ function, where
a register wasn't properly reset before re-use, cost several hours for
instance. Because the debugger with Atmel Studio is fairly limited, every
operation had to be traced by hand. This sometimes was quite frustrating.



\begin{thebibliography}{1}
  \bibitem{quark} J.~P.~Aumasson, L. Henzen, W. Meier and M Naya-Plasencia,
    \emph{\textsc{Quark}: a lightweight hash},
    Journal of Cryptology,
    April 2012. \url{https://131002.net/quark/quark_full.pdf}
\end{thebibliography}
\end{document}
% vim: set ts=4 sw=2 tw=0 et :
