;
;Exercise session 2, exercise 1: PRESENT Sbox
;
;Created: 04/02/2014 19:15:24
;  Author: Thom
;

.org 0x0200
sbox:
.db 0x0C, 0x05, 0x06, 0x0B, 0x09, 0x00, 0x0A, 0x0D, 0x03, 0x0E, 0x0F, 0x08, 0x04, 0x07, 0x01, 0x02

.org 0x0300
substitute:  ;subroutine
;precondition: r0 is first argument. Overwrites r16, r17, Z.
mov r16,r0
ldi ZH, high(sbox*2)
ldi ZL, low(sbox*2)
mov r17, r16 ; copy
andi r16, 0x0F ; grab the lower part
add ZL, r16 ; set sbox pointer to byte
lpm r16, Z
;; you could probably split it here.
ldi ZL, low(sbox*2) ;reset sbox pointer
andi r17, 0xF0
swap r17
add ZL, r17 ; set sbox pointer to desired byte
lpm r17, Z
swap r17
add r16, r17
mov r0, r16 ; set return value
reti

.org 0x0000 ;program
ldi r25, 20
mov r0, r25
rcall substitute ; sbox
mov r25, r0


end:
rjmp end
