/*
 * uQuark.asm
 *  
 *  Created: 08/03/2014 21:33:03
 *   Author: Thom Wiggers    (s4119444)
 *           Eduardo Novella (s4253043)
 *
 *  Goal         : High speed
 *  More info at : https://131002.net/quark/
 *                 http://rucryptoengineering.wordpress.com/
 *
 */ 

/** 
										+---------------+
										| Cycles count  |
										+---------------+

Main:
|
|
 --->(*) Init
|		 -load_padded_empty_msg_to_sram_0110                       2 #clocks  
|		 -initialize_registers                                    33 #clocks 
|
 --->(*) Absorb                                
|		 -4 operations + 1 RCALL + 1 X permutation  =  307371/318244 #clocks
|
 --->(*) Squeeze
|        -3 operations + 17*(8 ops + permute)      = 5225327/5410287 #clocks
----------------------------------------------------------------------------------
													5532733/5728566  #clocks

		 +------------------------------------------------------+
		 |TOTAL #CYCLES :        5532733/5728566  #clocks       |
		 +------------------------------------------------------+




(*) Permutation:  4 + 544*(565/585) = 307364/318244 #clocks
-4 operations
			---------------------------------------
		   /
		  /     fmacro		   209/217 #clocks
		 /      gmacro	       230/239 #clocks
        /       hmacro         108/111 #clocks
-544 * /			p operations         8 #clocks
	   \		extra ops           10 #clocks
	    \      --------------------------------
		 \                     565/585 #clocks
		  \
		   ----------------------------------------

*/


//****************************************************************************/
//  libraries
//****************************************************************************/
.include "tn45def.inc"

//****************************************************************************/
//  Own libraries
//****************************************************************************/
.include "utils.inc"
.include "fmacro.inc"
.include "gmacro.inc"
.include "hmacro.inc"
.include "linearequals.inc"

//****************************************************************************/
//  definitions
//****************************************************************************/
.def X0 = r0
.def X1 = r1
.def X2 = r2
.def X3 = r3
.def X4 = r4
.def X5 = r5
.def X6 = r6
.def X7 = r7
.def X8 = r8

.def Y0 = r10
.def Y1 = r11
.def Y2 = r12
.def Y3 = r13
.def Y4 = r14
.def Y5 = r15
.def Y6 = r16
.def Y7 = r17
.def Y8 = r18

.def L0 = r20
.def L1 = r21

.def T0 = r9  ; can be whereever
.def T1 = r24 ; needs to be high enough to LDI.
.def T2 = r19 ; can be whereever
.def T3 = r22 ; can be whereever
.def T4 = r23 ; can be whereever
.def i  = r25 

//****************************************************************************/
//  Main
//****************************************************************************/
.org 0x0000
rjmp main
	; message multiple 8 . First byte length of message (Bytes)
	msgLen:
	.db 0x00
	msg:
	.db 0x00

.org 0x0200

main:
	//****************************************************************************/
	// 1.- Init() and padding()
	//****************************************************************************/
	; We avoid to write 16 zero-bytes from 0100-0109 ==> we reduce 16*2 cycles
	load_padded_empty_msg_to_sram_0110

	; We keep everything in registers. We load the IV here, which is hardcoded
	initialize_registers


	//****************************************************************************/
	// 2.- absorb() 
	//****************************************************************************/
	/**  C code
	for(int i = 0; i < 1; i++) {
		for (int j = 0; j < 8; j++) //Just last byte 128-135
			state[128+j] ^= message[j]; 
		permute();
    }
	*/		

	; normally this should be a loop, looping n+1 times
	; where n is the message length.
	; The testvector is the empty message, so just absorb
	; the padding and permute once.
	LDI YH,0x01 ; Set pointer to the last byte of message
	LDI YL,0x10
	LD  T2, Y   ; message

	EOR Y8, T2  ; state[16] ^= Message
	RCALL permute


	//****************************************************************************/
	// 3.- squeeze()  
	//****************************************************************************/
	/**   C code
	for (int i = 0; i < 17;) {  
		out[i] = 0;
		for (int j = 0; j < 8; j++)
			out[i] ^= state[128+j] << (7-j);
		if (++i < 17) 
			permute();
	}
	*/
	CLR i        ; i = 0	
	LDI YH,0x01  ; Set pointer to out
	LDI YL,0x33
	for:
		ST  Y+, Y8   ; store last byte of state as final byte for the out[]
		RCALL permute
		INC i        ; i++
		CPI i, 0x11  ; ¿i == 17?
		BRNE for
		rjmp exit

	
	//****************************************************************************/
	// permute()  544 rounds with f,g,h,p  functions + extra xor's + updat p
	//****************************************************************************/
	permute:
		; permute init:
		LDI L0, 0x03
		LDI L1, 0xFF

		; abuse Z as cycle counter
		LDI ZL, 0x00
		LDI ZH, 0x00

		permute_loop:
			; Does the F function: rotates X and stores it's result in X[67]
			; note that X[67] then needs to be XORd with Y[0] and H
			fmacro

			; set last bit of X
			; X[67] = Y0 ^ ft ^ ht;
			; XOR X[67] with Y[0]
			BST Y0, 3
			BLD T4, 0
			EOR X8, T4

			; does the G function:  rotates Y and stores its result in Y[67]
			; Note that Y[67] still needs to be XORd with G and the result of H. (Y[67] = gt ^ ht;)
			gmacro

			; does the H function
			hmacro

			//pmacro
			;rotate L
			CLR T1
			LSL L1
			ROL L0
			BST L0, 2 ; L[0]
			BLD L1, 0
			BST L1, 7 ; L[3]
			BLD T1, 0
			EOR L1, T1
			;endp


			ADIW Z, 1
			CPI ZH, (512 >> 8) ; 512
			BRNE skipeq
				CPI ZL, 32     ; 32
				BRNE skipeq    ; == 544
					RET        ;return
			skipeq:
			RJMP permute_loop

			BREAK

exit:
.exit