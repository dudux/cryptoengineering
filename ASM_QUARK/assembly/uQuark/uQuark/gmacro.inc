/*
 *   gmacro.asm
 *
 *   Author: Eduardo Novella
 */ 

 /** Cycles count:
 AND 16
 XOR 25
 BST 18
 BLD 27
 LDI  9
 MOV  9
 -------
    -------------------->               104 #clocks
 9 X linear_equals   (13/14)*9      117/126 #clocks
 shift_9_registers_left                   9 #clocks

 +---------------------------------------------+
 |TOTAL #CYCLES :        230/239 #clocks       |
 +---------------------------------------------+
 */
.macro gmacro
	shift_9_registers_left Y0, Y1, Y2, Y3, Y4, Y5, Y6, Y7, Y8
	
	EOR T0, T0    ; reset this so we can use it for storage.
	EOR T1, T1
	EOR T2, T2
	EOR T3, T3 

	; !IMPORTANT the values are now in position n-1!
	; comments still use old positions though for documentation.
	BST Y0, 4 ; grab Y[0] ; ST reads, LD writes to register
	BLD Y8, 0 ; set it in the new position, it's 0 anyway so 0 XOR x = x

	; first do values that are not used again.
	; Y[16]
	BST Y2, 4  ; grab Y[16]
	BLD T0, 0  ; 
	EOR Y8, T0 ; Y[67] ^ Y[16]
	
	;Y[49]
	BST Y6, 3  ; grab Y[49]
	BLD T0, 0  ; 
	EOR Y8, T0 ; Y[67] ^ Y[49]

	; Start AND operations
	;Y[7] & Y[15]
	BST Y1, 5   ; grab Y[7]
	BLD T0, 0   ; STORE Y[7]IN T0[0]
	EOR Y8, T0  ; Y[67] ^ Y[7]
	
	; bit in T0   | bit in Y:
	; 0           | Y[7]
	; 1           | Y[20]
	; 2           | Y[30]
	; 3           | Y[35]
	; 4           | Y[37]
	; 5           | Y[42]
	; 6           | Y[51]
	; 7           | Y[54]

	; bit in T2   | bit in Y:
	; 0           | TEMPORARY LOADS


	; !IMPORTANT!
	; R9(T0) is now our storage for bits!!

	BST Y2, 5   ; grab Y[15]
	BLD T1, 0   ; store it in another temp register
	BLD T1, 7   ; store Y[15] in T1[7]
	AND T1, T0  ; Y[15] & Y[7]
	EOR Y8, T1  ; Y[67] ^ (Y[15] & Y[7])
	
	; Y[20]
	BST Y2, 0   ; grab Y[20]
	BLD T1, 0   ; 
	EOR Y8, T1  ; Y[67] ^ Y[20]
	BLD T0, 1   ; STORE Y[20] IN T0[1] 
	
	; Y[30]
	BST Y4, 6   ; grab Y[30]
	BLD T1, 0	;
	EOR Y8, T1  ; Y[67] ^ Y[30]
	BLD T0, 2   ; STORE Y[30] in T0[2]

	; Y[35]
	BST Y4, 1   ; grab Y[35]
	BLD T1, 0   ; 
	EOR Y8, T1  ; Y[67] ^ Y[35]
	BLD T0, 3   ; STORE Y[35] IN T0[3] 

	; Y[37]
	BST Y5, 7   ; grab Y[37]
	BLD T1, 0   ; 
	EOR Y8, T1  ; Y[67] ^ Y[37]
	BLD T0, 4   ; STORE Y[37] IN T0[4]

	; Y[42]
	BST Y5, 2   ; grab Y[42]
	BLD T1, 0   ; 
	EOR Y8, T1  ; Y[67] ^ Y[42]
	BLD T0, 5   ; STORE Y[42] IN T0[5]

	; Y[51]
	BST Y6, 1   ; grab Y[51]
	BLD T1, 0   ; 
	EOR Y8, T1  ; Y[67] ^ Y[51]
	BLD T0, 6   ; STORE Y[51] IN T0[6]

    ; Y[54]
	BST Y7, 6   ; grab Y[54]
	BLD T1, 0   ; 
	EOR Y8, T1  ; Y[67] ^ Y[54]
	BLD T0, 7   ; STORE X[54] IN T0[7]

	; Y[54] & Y[58]
	BST Y7, 2   ; grab Y[58]
	BLD T2, 0   ; 
	AND T1, T2  ; Y[54] & Y[58]                   
	EOR Y8, T1  ; Y[67] ^ (Y[58] & Y[54]) 


	; Y[35] & Y[37]
	; T0[3] & T0[4]
	LDI T1, ((1<<3)+(1<<4))
	MOV T3, T1 ; copy
	AND T1, T0 ; Y[35] & Y[37]
	linear_equals T1, T3
	EOR Y8, T1 ; Y[67] ^ (Y[35] & Y[37])
    

	; Y[42] & Y[51] & Y[54]
	; T0[5] & T0[6] & T0[7] 
	LDI T1, ((1<<5)+(1<<6)+(1<<7))
	MOV T3, T1 ; copy
	AND T1, T0
	linear_equals T1, T3
	EOR Y8, T1 ; Y[67] ^ (Y[42] & Y[51] & Y[54])


	; Y[20] & Y[30] & Y[35]
	; TO[1] & T0[2] & T0[3]
	LDI T1, ((1<<1)+(1<<2)+(1<<3))
	MOV T3, T1 ; copy
	AND T1, T0
	linear_equals T1, T3
	EOR Y8, T1 ; Y[67] ^ (Y[20] & Y[30] & Y[35])


	; Y[7]  & Y[30] & Y[42] & Y[58]    
	; T0[0] & T0[2] & T0[5] & extraload
	LDI T1, ((1<<0)+(1<<2)+(1<<5))
	MOV T3, T1 ; copy
	AND T1, T0 ; T1 = (Y[7] & Y[30] & Y[42])
	linear_equals T1, T3
	BST Y7, 2  ; grab Y[58]
	BLD T2, 0  ; STORE IN T2[0]
	AND T1, T2 ; T1 &= (Y[58])
	EOR Y8, T1 ; Y[67] ^ ( Y[7] & Y[30] & Y[42] & Y[58] )



	;       Y[35] & Y[37] & Y[51] & Y[54]
	; T[x]:   3   &   4   &   6   &    7
	LDI T1, ((1<<3)+(1<<4)+(1<<6)+(1<<7))
	MOV T3, T1 ; copy
	AND T1, T0
	linear_equals T1, T3
	EOR Y8, T1 ; Y[67] ^ (Y[35] & Y[37] & Y[51] & Y[54])



	;		Y[15] & Y[20] & Y[54] & Y[58]
	;      Y[20] & Y[54]    
	;  T0[x]  1  &   7 
	LDI T1, ((1<<1)+(1<<7)) 
	MOV T3, T1   ; copy
	AND T1, T0   ; T1 = Y[20] & Y[54] 
	linear_equals T1, T3
	;
	BST Y7, 2  ; grab Y[58]   
	BLD T2, 0  ; STORE IN T2[0]
	AND T1, T2 ; T1 &= Y[58]
	;
	BST Y2, 5  ; grab Y[15]
	BLD T2, 0  ; STORE IN T2[0]
	AND T1, T2 ; T1 &= Y[15]
	EOR Y8, T1 ; Y[67] ^ (Y[15] & Y[20] & Y[54] & Y[58])



	;       Y[37] & Y[42] & Y[51] & Y[54] (& Y[58])
	; T0[x]:  4   &   5   &   6   &    7
	LDI T1, ((1<<4)+(1<<5)+(1<<6)+(1<<7))
	MOV T3, T1 ; copy
	AND T1, T0 ; T1 = Y[37] & Y[42] & Y[51] & Y[54]
	linear_equals T1, T3
	BST Y7, 2  ; grab Y[58]   
	BLD T2, 0  ; STORE IN T2[0]
	AND T1, T2 ; T1 &= Y[58]
	EOR Y8, T1 ; Y[67] ^ (Y[37] & Y[42] & Y[51] & Y[54] & Y[58])



	;        Y[7] & Y[20] & Y[30] & Y[35] (& Y[15])
	; T0[x]:   0  &    1  &    2  &   3
	LDI T1, ((1<<0)+(1<<1)+(1<<2)+(1<<3))
	MOV T3, T1
	AND T1, T0 ; T1 = Y[7] & Y[20] & Y[30] & Y[35]
	linear_equals T1, T3
	BST Y2, 5  ; grab Y[15]
	BLD T2, 0  ; STORE IN T2[0]
	AND T1, T2 ; T1 &= Y[15]
	EOR Y8, T1 ; Y[67] ^ (Y[7] & Y[15] & Y[20] & Y[30] & Y[35])



	;     Y[20] & Y[30] & Y[35] & Y[37] & Y[42] & Y[51]
	; T[x]:  1  &    2  &   3   &    4  &    5  &    6
	LDI T1, ((1<<1)+(1<<2)+(1<<3)+(1<<4)+(1<<5)+(1<<6))
	MOV T3, T1 ; copy
	AND T1, T0
	linear_equals T1, T3
	EOR Y8, T1 ; Y[67] ^ (Y[20] & Y[30] & Y[35] & Y[37] & Y[42] & Y[51])

.endmacro