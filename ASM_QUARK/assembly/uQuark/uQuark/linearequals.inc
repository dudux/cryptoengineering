/*
 * linearequals.inc
 *
 *  Created: 21/03/2014 16:14:47
 *   Author: Thom Wiggers
 */ 

; stores result in first argument
.macro linear_equals ; register A, register B
EOR @0, @1
NEG @0
BRVS skiplinearequalsoverflow ; I don't know how else to take care of the overflow.
ROR @0    ; if this is just one operation, it still is time-invariant.
skiplinearequalsoverflow:
LSR @0
LSR @0
LSR @0
LSR @0
LSR @0
LSR @0
LSR @0
NEG @0
inc @0
.endm


.exit
// This test proves that the above code works.
TEST:
mov r2, r16
mov r3, r17
linear_equals r2, r3
BREQ skipnop
CP r16, r17
BREQ skipnop
nop
inc r15
skipnop:
inc r16
CPI r16, 0xFF
BRNE skipinc
inc r17
clr r16
skipinc:
CPI r17, 0xFF
BRNE skipbreak
break
skipbreak:
rjmp test