/*
 * fmacro.asm
 *
 *  Created: 08/03/2014 21:33:03
 *   Author: Thom Wiggers (s4119444)
 *
 */ 

 /** Cycles count:
 AND 16 
 XOR 25
 BST 16
 BLD 23
 LDI 8
 MOV 8
 -------
    -------------------->                96 #clocks
 8 X linear_equals   (13/14)*8      104/112 #clocks
 shift_9_registers_left                   9 #clocks

 +---------------------------------------------+
 |TOTAL #CYCLES :        209/217 #clocks       |
 +---------------------------------------------+
 */
.macro fmacro
	; first shift so we can already abuse the LSB of r8 as storage
	shift_9_registers_left X0, X1, X2, X3, X4, X5, X6, X7, X8
	
	EOR T0, T0    ; reset this so we can use it for storage.
	EOR T1, T1
	EOR T2, T2
	EOR T3, T3

	; !IMPORTANT the values are now in position n+1!
	; comments still use old positions though for documentation.
	BST X0, 4 ; grab X[0] ; ST reads, LD writes to register
	BLD X8, 0 ; set it in the new position, it's 0 anyway so 0 XOR x = x

	; first do values that are not used again.
	; X 14
	BST X2, 6 ; grab X[14]
	BLD T0, 0 ; 
	EOR X8, T0 ; X[67] ^ X[14]
	
	BST X6, 2 ; grab X[50]
	BLD T0, 0
	EOR X8, T0

	
	;X 9 and X 9 & X 15
	BST X1, 3 ; grab X[9]
	BLD T0, 0     ; STORE X[9] in T0[0] (equiv. andi T0, 1)
	EOR X8, T0    ; X[67] ^ X[9]
	; bit in T0   | bit in X:
	; 0           | X[9]
	; 1           | X[21]
	; 2           | 28
	; 3           | 33
	; 4           | 45
	; 5           | 52
	; 6           | 55
	; 7           | 59
	
	; !IMPORTANT!
	; R9 is now our storage for bits!!

	;do X 15 & X 9
	BST X2, 5   ; grab bit 15
	BLD T1, 0   ; store it in another temp register
	AND T1, T0  ; = X[9] & X[15]
	EOR X8, T1  ; X[67] ^ (X[9] & X[15]) 

	; X 21
	BST X3, 7    ; grab X[21]
	BLD T1, 0
	EOR X8, T1
	BLD T0, 1    ; STORE X[21] IN T0[1] 

	; XOR x[28]
	BST X3, 0     ; grab X[28]
	BLD T1, 0
	EOR X8, T1
	BLD T0, 2     ; STORE X[28] in r9[2]

	; XOR x[33]
	BST X4, 3     ; grab X[33]
	BLD T1, 0
	EOR X8, T1   ; XOR with X[67] 
	BLD T0, 3     ; keep around for later T0[3]
	
	;second part: x33&x37
	BST X5, 7     ; grab X[37]
	BLD T2, 0
	AND T1, T2
	EOR X8, T1

	; XOR X[45]
	BST X6, 7    ; grab X[45]
	BLD T1, 0
	EOR X8, T1
	BLD T0, 4    ; store in T0

	; XOR X[52]
	BST X6, 0    ; grab X[52]
	BLD T1, 0
	EOR X8, T1
	BLD T0, 5    ; store in T0

	; XOR X[55] and X[55]&X[59]
	BST X7, 5      ; grab X55
	BLD T1, 0
	BLD T0, 6      ; store in T0
	EOR X8, T1  
	; grab X59 for AND
	BST X7, 1      ;grab X[59]
	BLD T2, 0
	AND T1, T2
	EOR X8, T1  ; X[55] & X[59]

	; X45 & X52 & X55 =
	; T0[4] & T0[5] & T0[6] 
	LDI T1, ((1<<4)+(1<<5)+(1<<6))
	MOV T3, T1 ; copy
	AND T1, T0
	linear_equals T1, T3
	EOR X8, T1

	; X21 & X28 & X33 =
	; T0[1] & T0[2] & T0[3] 
	LDI T1, ((1<<1)+(1<<2)+(1<<3))
	MOV T3, T1
	AND T1, T0
	linear_equals T1, T3
	EOR X8, T1

	; LOAD X59 into T0[7]
	BST X7, 1
	BLD T0, 7

	; X9 & X28 & X45 & X59 =
	; T0[0] & T[2] & T0[4] & T0[7] 
	LDI T1, ((1<<0)+(1<<2)+(1<<4)+(1<<7))
	MOV T3, T1
	AND T1, T0
	linear_equals T1, T3
	EOR X8, T1

	; LOAD bit 37
	BST X5, 7
	BLD T2, 0     ; IF T2 isn't empty: PROBLEMS!
	EOR X8, T2    ; XOR with X37

	; test 33, 52, 55 (& 37 (1 here))
	; T0[3], T0[5], T0[6]
	LDI T1, ((1<<3)+(1<<5)+(1<<6))
	MOV T3, T1
	AND T1, T0
	linear_equals T1, T3
	AND T1, T2
	EOR X8, T1

	;         X[45] & X[52] & X[55] & X[59]; (&X[37])
	; T0[x]:     4       5       6     7 
	LDI T1, ((1<<4)+(1<<5)+(1<<6)+(1<<7))
	MOV T3, T1
	AND T1, T0
	linear_equals T1, T3
	AND T1, T2
	EOR X8, T1

	; X[21] & X[28] & X[33]  & X[45] & X[52]; (& X[37])
	; bit 1   2       3         4       5
	LDI T1, ((1<<1)+(1<<2)+(1<<3)+(1<<4)+(1<<5))
	MOV T3, T1
	AND T1, T0
	linear_equals T1, T3
	AND T1, T2
	EOR X8, T1

	; LOAD X 15 in T flag
	BST X2, 5
	BLD T2, 0

	;        X[21] & X[55] & X[59]; (& X15)
	; T0[x]:   1       6        7
	LDI T1, ((1<<1)+(1<<6)+(1<<7))
	MOV T3, T1
	AND T1, T0
	linear_equals T1, T3
	AND T1, T2
	EOR X8, T1

	;        X[9]  & X[21] & X[28] & X[33];  (& X[15])
	; T0[x]:   0      1        2      3
	LDI T1, ((1<<0)+(1<<1)+(1<<2)+(1<<3))
	MOV T3, T1
	AND T1, T0
	linear_equals T1, T3
	AND T1, T2
	EOR X8, T1
.endmacro