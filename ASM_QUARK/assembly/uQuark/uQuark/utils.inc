; Utility functions, like loading from registers

; Direct add
/**
 +---------------------------------------------+
 |TOTAL #CYCLES :        1 #clocks             |
 +---------------------------------------------+
 */
.macro addi
	subi @0, -(@1)
.endmacro

; BRANCH IF ZERO SET
/**
 +---------------------------------------------+
 |TOTAL #CYCLES :        1/2 #clocks           |
 +---------------------------------------------+
 */
.macro BRZS
	brbs 1, @0
.endmacro

// Load empty message = {} => 1000 0000 => 0x80
/**
 +---------------------------------------------+
 |TOTAL #CYCLES :        2 #clocks             |
 +---------------------------------------------+
 */
.macro 	load_padded_empty_msg_to_sram_0110
	ldi r16,0x80
	sts $0110,r16
.endmacro

 /** Cycles count:
 LSL 1
 ROL 8
 +---------------------------------------------+
 |TOTAL #CYCLES :        9 #clocks             |
 +---------------------------------------------+
 */
.macro shift_9_registers_left; rp .. r(p+8)
	LSL @8 ; shift bit to the left so we get a carry
	ROL @7 
	ROL @6
	ROL @5
	ROL @4
	ROL @3
	ROL @2
	ROL @1
	ROL @0
.endmacro

// Initialization with the Initial Vector (IV)
/**
LDI 18
MOV 15
 +---------------------------------------------+
 |TOTAL #CYCLES :        33 #clocks            |
 +---------------------------------------------+
 */
.macro initialize_registers
	; load X
	ldi r16, 0x0D
	mov X0, r16
	ldi r16, 0x8D
	mov X1, r16
	ldi r16, 0xAC
	mov X2, r16
	ldi r16, 0xA4
	mov X3, r16
	ldi r16, 0x44
	mov X4, r16
	ldi r16, 0x14
	mov X5, r16
	ldi r16, 0xA0
	mov X6, r16
	ldi r16, 0x99
	mov X7, r16
	ldi r16, 0x71
	mov X8, r16

	; load Y
	ldi r16, 0x09
	mov Y0, r16
	ldi r16, 0xC8
	mov Y1, r16
	ldi r16, 0x0A
	mov Y2, r16
	ldi r16, 0xA3
	mov Y3, r16
	ldi r16, 0xAF
	mov Y4, r16
	ldi r16, 0x06
	mov Y5, r16
	ldi Y6, 0x56 ; from Y6 we can load directly
	ldi Y7, 0x44 ; TODO align Y better so we can load more directly.
	ldi Y8, 0xDB
.endm