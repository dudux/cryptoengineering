Repository for the Crypto-Engineering assignments subject at Radboud Nijmegen. 
==============================================================================

Objectives:
-----------

* 1) ASM_QUARK. AVR-8bit assembler implementation for QUARK algorithm  with high speed in mind
* 2) SCA_PRESENT. Shuffling in PRESENT to withstand Side Channel Attacks (SCA)

References
----------
More details at : 
http://rucryptoengineering.wordpress.com/
Radboud University Nijmegen (Digital Security and The Kerckhoffs Institute)